import React from "react";
import { Grid, Typography } from "@mui/material";
import Image from "next/image";

const About = ({ cssId }) => {
  return (
    <div id={cssId}>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        className="about-grid"
        style={{ margin: "1% auto" }}
      >
        <Grid item xs={12} lg={6} style={{ textAlign: "center" }}>
          <div id="textSection">
            <Typography variant="h1" align="left" style={{ margin: "4% auto" }}>
              About
            </Typography>

            <Typography
              variant="body1"
              align="left"
              gutterBottom
              style={{ margin: "1% auto" }}
            >
              Versatile Fullstack Developer.
              <br />
              10+ Years Crafting Telecoms Backend Solutions in C++ & Java.
              <br />
              Microsoft XNA Ireland Game Development Challenge Winner.
            </Typography>
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          lg={6}
          justifyContent="center"
          alignItems="center"
          style={{ textAlign: "center" }}
        >
          <Image
            src="/images/code.webp"
            width="2230"
            height="1569"
            layout="responsive"
            style={{ margin: "10% auto", borderRadius: "10px" }}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default About;
