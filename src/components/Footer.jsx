import React from "react";
import { Typography } from "@mui/material";
import { css } from "@emotion/css";
import { useTheme } from "@mui/material/styles";

const Footer = () => {
  const theme = useTheme();

  const xstyles = css`
    background-color: ${theme.palette.primary.main};
    min-height: 10%;
    margin-top: 5%;
    display: flex;
    position: absolute;
    width: 100%;
  `;

  return (
    <div className={xstyles}>
      <Typography
        variant="subtitle1"
        align="center"
        style={{ margin: "1% auto" }}
      >
        &copy; 2023 tplsofteng. All rights reserved.
      </Typography>
    </div>
  );
};

export default Footer;
