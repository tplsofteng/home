import { React, useState, useEffect } from "react";
import { css } from "@emotion/css";
import { useTheme } from "@mui/material/styles";
import { CircularProgress } from "@mui/material";

const LoadingScreen = ({ timer, children }) => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // Simulate loading data or fetching API
    setTimeout(() => {
      setIsLoading(false);
    }, timer); // Simulated loading time
  }, []);

  const theme = useTheme();

  const xstyles = css`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;

    display: flex;
    align-items: center;
    justify-content: center;

    position: absolute;
  `;

  return (
    <>
      {isLoading && (
        <div className={xstyles}>
          <CircularProgress value={40} />
        </div>
      )}

      <div className={isLoading ? "hidden" : ""}> {children}</div>
    </>
  );
};

export default LoadingScreen;
