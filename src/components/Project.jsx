import { Button, Typography } from "@mui/material";
import Image from "next/image";
import { css } from "@emotion/css";
import { useTheme } from "@mui/material/styles";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";

const Project = ({ image, title, demo, link, children }) => {
  const theme = useTheme();

  const xstyles = css`
    background-color: ${theme.palette.secondary.main};
    padding: 40px;
    border-radius: 8px;
    min-height: 320px;
  `;

  return (
    <Card className={xstyles}>
      {/* Bind all elements inside card */}
      <Box
        alignItems="center"
        justifyContent="center"
        display="flex"
        flexDirection="column"
      >
        {/* Image */}
        <Box
          alignItems="center"
          justifyContent="center"
          display="flex"
          flexDirection="column"
        >
          <Image src={image} width={320} height={180} alt="{title} image" />
        </Box>

        {/* Title, button and description */}
        <Box>
          <Box display="flex" style={{ margin: "5% 0 0 0%" }}>
            <Typography variant="h4">{title}</Typography>
            {demo ? (
              <Button href={demo} component="button" variant="body2">
                Demo
              </Button>
            ) : null}
            {link ? (
              <Button href={link} component="button" variant="body2">
                Link
              </Button>
            ) : null}
          </Box>
          <Box alignItems="center" justifyContent="center">
            {children}
          </Box>
        </Box>
      </Box>
    </Card>
  );
};

export default Project;
