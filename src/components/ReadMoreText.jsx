/**
 * ReadMoreText.jsx
 *
 * Description: Trims enclosed text to specified size.
 *              Also provides an expand/contract button.
 *
 * Props:
 * - maxChars (int): The maxmimum number of characters to display in contracted mode.
 */

import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

function ReadMoreText({ children, maxChars, variant }) {
  const [showMore, setShowMore] = useState(false);

  const toggleShowMore = () => {
    setShowMore(!showMore);
  };

  /**
   * Prune textual node to maximum of maxChars size
   * @returns pruned elements array
   */
  const pruneContent = () => {
    let contentLen = 0;
    const prunedContent = [];

    for (let i = 0; i < children.length; i++) {
      let child = children[i];

      if (typeof child === "string") {
        let remainingBudget = maxChars - contentLen;
        let maxLen = Math.min(remainingBudget, child.length);
        prunedContent.push(child.slice(0, maxLen));
        contentLen += maxLen;
      } else {
        prunedContent.push(child);
      }

      if (contentLen >= maxChars) {
        break;
      }
    }

    return prunedContent;
  };

  return (
    <div>
      <Typography variant={variant}>
        {showMore ? children : pruneContent(children)}
      </Typography>

      <Button onClick={toggleShowMore} color="primary">
        {showMore ? "Read Less" : "Read More"}
      </Button>
    </div>
  );
}

export default ReadMoreText;
