import { useState, useRef, useEffect } from "react";
import { css } from "@emotion/css";
import { useTheme } from "@mui/material/styles";
import { Stepper, Step, StepLabel } from "@mui/material";

const SideBar = ({ cssId, watch }) => {
  const theme = useTheme();

  const xstyles = css`
    position: fixed;
    top: 50%; /* Center the sidebar vertically */
    left: 0;
    transform: translateY(-50%); /* Center the sidebar vertically */
    min-width: 80px;
    padding: 16px;
    background-color: #f0f0f0;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  `;

  const handleIntersection = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        let id = "#" + entry.target.attributes.getNamedItem("id").value;

        watch.forEach((name, idx) => {
          if (name === id) {
            setActiveStep(idx + 1);
          }
        });
      }
    });
  };

  const [activeStep, setActiveStep] = useState(0);

  useEffect(() => {
    const observer = new IntersectionObserver(handleIntersection, {
      root: null,
      rootMargin: "0px",
      threshold: 0.5,
    });

    watch.forEach((elem) => {
      const target = document.querySelector(elem);
      if (target) {
        console.log("observing " + elem);
        observer.observe(target);
      }
    });
  });

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <div className={xstyles} id={cssId}>
      <Stepper orientation="vertical" activeStep={activeStep}>
        <Step>
          <StepLabel>About</StepLabel>
        </Step>
        <Step>
          <StepLabel>Skills</StepLabel>
        </Step>
        <Step>
          <StepLabel>Projects</StepLabel>
        </Step>
      </Stepper>
    </div>
  );
};

export default SideBar;
