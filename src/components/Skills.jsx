import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import {
  FaJava,
  FaReact,
  FaAws,
  FaPython,
  FaDocker,
  FaLinux,
} from "react-icons/fa";

const Skills = ({ cssId }) => {
  return (
    <div id={cssId}>
      <Typography
        variant="h2"
        align="center"
        gutterBottom
        style={{ margin: "10% auto" }}
      >
        Skills I focus on today.
      </Typography>

      <Container>
        <Grid
          container
          spacing={2}
          justifyContent="center"
          className="skills-grid"
        >
          <Grid item>
            <FaJava size={50} />
            <div>Java</div>
          </Grid>
          <Grid item>
            <img src="/images/SimpleIconsSpring.svg" alt="spring" />
            <div>Spring</div>
          </Grid>
          <Grid item>
            <FaReact size={50} />
            <div>React</div>
          </Grid>
          <Grid item>
            <img src="/images/TeenyiconsNextjsSolid.svg" alt="spring" />
            <div>Next.js</div>
          </Grid>
          <Grid item>
            <FaAws size={50} />
            <div>AWS</div>
          </Grid>
          <Grid item>
            <FaPython size={50} />
            <div>Python</div>
          </Grid>
          <Grid item>
            <FaDocker size={50} />
            <div>Docker</div>
          </Grid>
          <Grid item>
            <FaLinux size={50} />
            <div>Linux</div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Skills;
