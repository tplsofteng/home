import { createTheme } from "@mui/material/styles";

const theme = createTheme({
    palette: {
      primary: {
        main: '#1e81b080', // Icy blue color
      },
      secondary: {
        main: '#FFFFFF80', // White color
      },
      background: {
        default: '#E1F5FE', // Light blue background color
        paper: '#FFFFFF', // White background for paper elements
      },
      text: {
        primary: '#333333', // Dark text color (instead of white)
        secondary: '#B3E5FC', // Light blue secondary text color
      },
    },
    typography: {
      fontFamily: 'Roboto, Arial, sans-serif',
      h1: {
        fontSize: '3rem',
        fontWeight: 600,
        letterSpacing: '0.1rem',
        textTransform: 'uppercase',
        color: '#333333', // Dark text color (instead of white)
      },
      h2: {
        fontSize: '2.5rem',
        fontWeight: 500,
        letterSpacing: '0.1rem',
        color: '#333333', // Dark text color (instead of white)
      },
      h3: {
        fontSize: '2rem',
        fontWeight: 400,
        color: '#333333', // Dark text color (instead of white)
      },
      body1: {
        fontSize: '1rem',
        fontWeight: 300,
        color: '#333333', // Dark text color (instead of white)
      },
      button: {
        textTransform: 'uppercase',
        fontWeight: 500,
      },
    },
    overrides: {
      MuiButton: {
        root: {
          borderRadius: 20,
          padding: '8px 16px',
        },
        containedPrimary: {
          background: 'linear-gradient(45deg, #29B6F6 30%, #4FC3F7 90%)', // Gradient for primary buttons
          color: '#333333', // Dark text color (instead of white)
        },
        outlinedPrimary: {
          border: '2px solid #4FC3F7', // Border for primary outlined buttons
          color: '#4FC3F7', // Icy blue color for the text (instead of white)
        },
      },
    },
  });

  export default theme;