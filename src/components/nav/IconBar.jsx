import React from 'react';
import Image from 'next/image';
import { useTheme } from '@emotion/react';
import { css } from '@emotion/css';

const IconBar = () => {

    const theme = useTheme();

    const xstyles = css`
    display: flex;
    text-align: center;

    img{
        vertical-align: middle;
    }
  `;

    return (
        <div className={xstyles}>
            <a href="https://linkedin.com/in/tony-lowry">
                <Image src={'/images/LI-In-Bug.png'} alt="linkedin" width={40} height={32}></Image>
            </a>
            <a href="https://gitlab.com/tplsofteng">
                <Image src={'/images/gitlab-logo-700.png'} alt="gitlab"width={40} height={32}></Image>
            </a>
        </div>
    )
}




export default IconBar;