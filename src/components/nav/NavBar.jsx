import React from "react";
import { css } from "@emotion/css";
import { useTheme } from "@mui/material/styles";
import Link from "next/link";
import IconBar from "./IconBar";
import Image from "next/image";
import { Box, Typography } from "@mui/material";

const Navbar = () => {
  const theme = useTheme();

  const localStyle = css`
    display: flex;
    background-color: ${theme.palette.primary.main};
    color: ${theme.palette.primary.contrastText};
    padding: 2px;
    max-height: 60px;
    align-items: center;
    justify-content: space-between;

    #rightItems li {
      display: flex;
      align-items: center;
    }

    #rightItems ul {
      display: flex;
    }

    #leftTitle {
      display: flex;
    }
  `;

  return (
    <nav className={localStyle}>
      <div id="leftItems">
        <Link href="/" id="leftTitle">
          <Image src="/images/owl_thumb.png" width={48} height={48}></Image>
          <Box display="flex" justifyContent="center" alignItems="center">
            <Typography variant="subtitle1">TPL</Typography>
          </Box>
        </Link>
      </div>
      <div id="rightItems">
        <ul>
          <li>
            <Link href="https://tplsofteng.gitlab.io/blog/">Blog</Link>
            <Link href="/tools">Tools</Link>
          </li>
          <IconBar />
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
