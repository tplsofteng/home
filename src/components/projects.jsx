import React from "react";
import Project from "./Project";
import ReadMoreText from "./ReadMoreText";
import { Grid, Typography } from "@mui/material";

const Projects = ({ cssId }) => {
  return (
    <div id={cssId}>
      <Typography variant="h2" align="center" style={{ margin: "4% auto" }}>
        Projects I have worked on
      </Typography>
      <Grid container spacing={10}>
        <Grid item xs={12} lg={6}>
          <Project
            title="Language Cards"
            demo="https://red-wave-0dc324e03.3.azurestaticapps.net/"
            image="/images/projects/cartes.webp"
          >
            <Typography variant="h5">
              A French language card game. using CSS3 & HTML5 <br />
            </Typography>
          </Project>
        </Grid>
        <Grid item xs={12} lg={6}>
          <Project
            title="Rollover Board"
            link="https://gitlab.com/rollover-board"
            image="/images/projects/code-markus-spiske-cvBBO4PzWPg-unsplash.webp"
          >
            <ReadMoreText variant="h5" maxChars="34">
              •Messageboard demonstrator system.
              <br />
              •Containerized microservice architecture
              <br />
              •Tensorflow Machine learning spam filter
              <br />
              •grpc interconnect protocol
              <br />
              •react frontend
              <br />
              •deployed using kubernetes
              <br />
            </ReadMoreText>
          </Project>
        </Grid>
        <Grid item xs={12} lg={6}>
          <Project
            title="Grawl"
            link="https://gitlab.com/tplsofteng/grawl"
            image="/images/projects/code-markus-spiske-cvBBO4PzWPg-unsplash.webp"
          >
            <Typography variant="h5">
              Web scraping library from scratch in golang
            </Typography>
          </Project>
        </Grid>
        <Grid item xs={12} lg={6}>
          <Project
            title="Gamecorral"
            link="https://gitlab.com/tplsofteng/gamecorral"
            image="/images/projects/code-markus-spiske-cvBBO4PzWPg-unsplash.webp"
          >
            <Typography variant="h5">JS Game API & Hosting service</Typography>
          </Project>
        </Grid>
      </Grid>
    </div>
  );
};

export default Projects;
