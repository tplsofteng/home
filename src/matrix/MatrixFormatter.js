import React, { useState } from 'react';
import { TextField, Button, Grid } from '@mui/material';
import { extractArray, extractDecimals } from './MatrixTools';

const MatrixFormatter = () => {
  const [inputText, setInputText] = useState('');
  const [outputText, setOutputText] = useState('');

  /* Formats a text matrix into a more readable layout*/
  const formatMatrix = () => {
    try {

      let ans = extractArray(inputText);
      let arr = ans['arr'];
      let max = ans['max'];

      let maxDecimals = extractDecimals(max);
      let out = '';

      for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < arr[0].length; j++) {
          let num = arr[i][j];
          let dec = extractDecimals(num)
          let diff = maxDecimals - dec;

          // Account for minus symbol
          if (num < 0){
            diff-=1;
          }
          // Add padding for the missing decimal places
          for (let k = 0; k < diff; k++)
            out += ' ';
          
          out += ' ' + num;

        };
        out += '\n';
      }
      setOutputText(out);

    } catch (err) {
      setOutputText('Error : ' + err);
    }
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        {/* Input Text Box */}
        <TextField
          fullWidth
          label="Input Text"
          variant="outlined"
          value={inputText}
          onChange={(e) => setInputText(e.target.value)}
        />
      </Grid>
      <Grid item xs={12}>
        {/* Output Text */}
        <pre>
          {outputText}
        </pre>
      </Grid>
      <Grid item xs={12}>
        {/* Transform Button */}
        <Button variant="contained" color="primary" onClick={formatMatrix}>
          Format
        </Button>
      </Grid>
    </Grid>
  );
};

export default MatrixFormatter;