export const extractDecimals = (num) => {

    num = Math.abs(num);

    if (num < 10){
        return 1;
    }

    let decimals = 0;
    
    while (num > 0) {
        num = Math.floor(num / 10);
        decimals++;
    }
    return decimals;
}

/* Extract a 2D array from the input text*/
export const extractArray = (text) => {

    if (text.length === 0){
        throw 'Please enter a valid 2d array';
    }

    let ans = null;
    let st = []
    let x = 0;
    let y = 0;

    var digit = ""; // accumulate digits
    var maxNum = 0;

    for (let i = 0; i < text.length; i++) {
        let ch = text[i];
        if (ch === '[') {
            st.push(ch);
            if (ans == null) {
                ans = []
            } else {
                // first subarray, don't increment
                if (ans.length == 0){
                    ans.push([]);
                }
                else if (st.length < 3){
                    ans.push([]);
                    x++;
                }else{
                    throw 'Only 2D arrays are supported';
                }
            }
            
            y = 0;

        } else if (ch === ']') {
            if (st.length == 0) {
                throw 'Unmatched ] at ' + i;
            }
            st.pop();

            // last item in array
            if (i == text.length - 1) {
                var tmpDigit = parseInt(digit);
                maxNum = Math.max(maxNum, tmpDigit);
                ans[x][y] = tmpDigit;
                digit = "";
            }
        }

        else if (ch === ',') {
            if (ans.length == 0) {
                throw 'Only 2D arrays are supported';
            
            }
            var tmpDigit = parseInt(digit);
            maxNum = Math.max(maxNum, tmpDigit);
            ans[x][y] = tmpDigit;
            digit = "";

            y++;
        } else {
            digit += ch;
        }
    }

    return { max: maxNum, arr: ans };
}