import React, { useState, useEffect } from 'react';
import { ThemeProvider } from "@mui/material/styles";

import Navbar from '../components/nav/NavBar';

import theme from '../components/Theme';


import '../styles/styles.scss'
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import Footer from '@/components/Footer';


function MyApp({ Component, pageProps }) {
  // Propagate the Navbar component to all pages
  return (
    <>

      <ThemeProvider theme={theme}>
        <Navbar />

        <div id="content" >

          <Component {...pageProps} />


        </div>

        <Footer></Footer>
      </ThemeProvider>

    </>
  );
}

export default MyApp;