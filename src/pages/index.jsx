import { React } from 'react';

import { Box, Divider, Grid } from '@mui/material';
import { Fade } from "react-awesome-reveal";
import LoadingScreen from '@/components/LoadingScreen';

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import Projects from '@/components/projects';
import Skills from '@/components/Skills';
import About from '@/components/About';
import SideBar from '@/components/SideBar';

const Index = () => {
  return (
    <div id="index">
      <LoadingScreen timer={500}>
        <SideBar cssId="sidebar" watch={['#about', '#skills', '#projects']} ></SideBar>
        <Grid container alignItems="center" justifyContent="center" >
          <Grid item xs={12} md={11} >
            <Fade cascade={true} fraction={0.1}>
              <About cssId="about" ></About>
              <Box style={{ textAlign: 'center', margin: '2% auto' }}><KeyboardArrowDownIcon fontSize="large" className='inviteDown' /></Box>
            </Fade>

            <Divider style={{ margin: '12% auto' }} />

            <Fade cascade={true} fraction={0.1}>
              <Skills cssId="skills"></Skills>
            </Fade>

            <Divider style={{ margin: '12% auto' }} />

            <Fade cascade={true} fraction={0.1} >
              <Projects cssId="projects" style={{ margin: '12% auto' }} />
            </Fade>
          </Grid>
        </Grid>
        </LoadingScreen >
    </div>
    
  );
};

export default Index;