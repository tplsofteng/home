import React from 'react';
import { Typography, List, ListItem, ListItemIcon, ListItemText, Box } from '@mui/material';
import CodeIcon from '@mui/icons-material/Code';

const Tools = () => {
  return (
    <>
      <Typography variant="h2"  style={{ margin: '2% auto', textAlign : 'center' }}>
        Tools
      </Typography>
      <Box display="flex"
        justifyContent="center"
        alignItems="center">
        <List component="nav">
          <ListItem component="a" href="/tools/matrix">
            <ListItemIcon>
              <CodeIcon />
            </ListItemIcon>
            <ListItemText primary="2d Matrix Formatter" />
          </ListItem>
          <ListItem component="a" href="/tools/matrix">
            <ListItemIcon>
              <CodeIcon />
            </ListItemIcon>
            <ListItemText primary="Coming Soon" />
          </ListItem>
        </List>
      </Box>
    </>
  );
};

export default Tools;