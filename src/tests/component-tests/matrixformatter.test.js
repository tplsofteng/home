import { extractArray, extractDecimals } from '../../matrix/MatrixTools';

test('extractDecimals', () => {
    expect(extractDecimals(-1)).toBe(1);
    expect(extractDecimals(0)).toBe(1);
    expect(extractDecimals(9)).toBe(1);
    expect(extractDecimals(-10)).toBe(2);
    expect(extractDecimals(214)).toBe(3);    
});

test('extractArray', () => {
    var arr = extractArray('[[1, 2, 3],[4, 5, 6]]');
    expect(arr['arr']).toEqual([[1, 2, 3],[4, 5, 6]]);
    expect(arr['max']).toEqual(6);

    expect(() => extractArray('[1, 2, 3]')).toThrow('Only 2D arrays are supported');
    expect(() => extractArray('[[[1,2,3]]]')).toThrow('Only 2D arrays are supported');
});
